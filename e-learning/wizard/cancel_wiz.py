# -*- coding: utf-8 -*-

from odoo import models ,fields , api
from odoo.exceptions import except_orm, Warning, ValidationError


class cancelCourseWiz(models.TransientModel):
    _name = 'cancel.course.wiz'

    @api.multi
    def cancel_course(self):
        course_obj =  self.env['elearning.course'].browse(self._context['active_ids'])
        for i in course_obj:
            if i.nb_registration > 0 :
                raise ValidationError('This course has a students')
            else:
                i.write({
                    'state': 'cancel'
                })


