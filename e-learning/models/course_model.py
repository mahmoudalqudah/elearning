from odoo import api, models, fields


class Courses(models.Model):
    _name = "elearning.course"

    img = fields.Binary("Image", attachment=True)
    name = fields.Char("Course Name")
    description = fields.Text("Description")
    enrolment_ids = fields.One2many('elearning.enrolment' ,'course_id' , string = "Enrolment")
    state = fields.Selection([('draft','Draft'),('cancel','Cancel')] ,default = "draft")

    nb_registration = fields.Integer('Students number', compute='_compute_registration')
    price = fields.Integer('Price')
    total_price = fields.Integer(compute="_compute_total_price")



    @api.one
    def _compute_registration(self):
        self.nb_registration = self.env['elearning.enrolment'].search_count([('id', 'in', self.enrolment_ids.ids)])
    @api.one
    def _compute_total_price(self):
        if self.price:
            self.total_price = len(self.enrolment_ids) * self.price
