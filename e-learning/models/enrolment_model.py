from odoo import api , models , fields

class Enrolment(models.Model):
    _name = "elearning.enrolment"
    name = fields.Char('name')
    course_id = fields.Many2one('elearning.course' , "Course Name" ,required=True)
    student_ids = fields.Many2one('res.partner' , "Student" , domain= [('user_type', '=' , 'student')])
    teacher_ids = fields.Many2one('res.partner', "Teacher", domain=[('user_type', '=', 'teacher')])

    state = fields.Selection([('draft' , 'Draft') , ('confirmed', 'Confirmed') ,('paid' ,'Paid') ,('cancel','Cancel')] , default = 'draft' , string ="Status")




    # @api.model
    # def create(self, vals):
    #     rec = super(Enrolment, self).create(vals)
    #     # ...
    #     course_obj = self.env['elearning.course'].search([('id','=',self.course_id.id)],limit=1)
    #     course_obj.course_count += 1
    #     print(course_obj.course_count)
    #
    #     return rec
