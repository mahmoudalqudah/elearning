from odoo import models , api , fields
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT


class Users(models.Model):

    _inherit = "res.partner"

    user_type = fields.Selection([('student','Student'),('teacher','Teacher')], help = "User Type" )
    date_reg = fields.Date(string="Date", required=True, default=fields.Date.today)
    count_reg_student = fields.Integer(compute='_display_count_student')
    count_reg_teacher = fields.Integer(compute='_display_count_teacher')
    @api.one
    def _display_count_student(self):
        for rel in self:
            count_id = self.env['elearning.enrolment'].search([('student_ids', '=', rel.id)])
            self.count_reg_student = len(count_id)

    @api.one
    def _display_count_teacher(self):
        for rel in self:
            count_id = self.env['elearning.enrolment'].search([('teacher_ids', '=', rel.id)])
            self.count_reg_teacher = len(count_id)

    @api.multi
    def get_courses_student(self):
        return {
            'name': 'Courses',
            'domain': [('student_ids' , '=' , self.id)],
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'tree,list',
            'res_model': 'elearning.enrolment',
            'target': 'new',
        }

    @api.multi
    def get_courses_teacher(self):
        return {
            'name': 'Courses',
            'domain': [('teacher_ids', '=', self.id)],
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'tree,list',
            'res_model': 'elearning.enrolment',
            'target': 'new',
        }